import * as actionTypes from '../../Contaniers/Action/Action';

const initialState = {   
    count: 0,
    items: [
          {
            id: 1,
            image: "./image/item-1.png",
            name: "Common Projects",
            details: "Bball High",
            color: "White",
            product_price: 549
          },
          {
            id: 2,
            image: "./image/item-2.png",
            name: "Maison Margiela",
            details: "Future Sneakers",
            color: "White",
            product_price: 870
          },
          {
            id: 3,
            image: "./image/item-3.png",
            name: "Our Legacy",
            details: "Brushed Scarf",
            color: "Brown",
            product_price: 349
          }
        ]
      
    
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {         
        case actionTypes.DELETEITEMS:
            return {              
                ...state,
                items: state
                .items.filter(item => item.id !== action.id)  
                           
        }
        // case actionTypes.DECREMENTCOUNT:
        //     return {
        //         ...state,
        //         count: state.count -1 
        //     }
        // case actionTypes.DELETEITEMS:
        //     return {
        //         ...state,
        //         counter: state.counter + action.val
        //     }
        
    }
    return state;
};

export default reducer;