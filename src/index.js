import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {Provider} from 'react-redux';
import {createStore,combineReducers} from 'redux'
import itemReducer from './Contaniers/Reducer/Items';

import { BrowserRouter } from "react-router-dom";
import { Switch, Route } from "react-router-dom";

import News from './component/News/News';
import Contact from './component/Contact/Contact';
import About from './component/About/About';
import Menu from './component/Menu/Menu';

const rootReducer = combineReducers({
  ctr: itemReducer  
});

const store=createStore(rootReducer);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
        <main>
          <Menu/>
          <Switch>
            <Route exact path='/' component={App} />
            <Route exact path='/News' component={News} />
            <Route exact path='/Contact' component={Contact} />
            <Route exact path='/About' component={About} />
          </Switch>
          
        </main>
      </BrowserRouter>
    </Provider>,
   
   document.getElementById("root")
  );



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
