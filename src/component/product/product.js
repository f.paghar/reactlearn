import React, { Component } from "react";
import './product.css'
import plus from "../../Assist/plus.svg";
import minus from "../../Assist/minus.svg";

class Product extends Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 1,
      likeclass: "like-btn"
    };

    this.overlike = this.overlike.bind(this);
    this.leavelike = this.leavelike.bind(this);
    this.deletehandeler = this.deletehandeler.bind(this);
  }

  handleIncrement = count => {
    if (count <= 4) {
      this.setState({
        count: count + 1
      });
    }
  };

  handleDecrement = (id, count) => {
    if ((count >= 2) & (count <= 5)) {
      this.setState({
        count: count - 1
      });
    }

    if ((count === 1)) {     
      this.props.action(this.props.item.id);
    }
  };

  Totallprice = (price, count) => {
    return price * count < 0 ? "invalid input" : "$" + price * count;
  };

  overlike(event) {
    this.setState({ likeclass: "is-active" });
  }

  leavelike(event) {
    this.setState({ likeclass: "like-btn" });
  }

  deletehandeler() {
    this.props.action(this.props.item.id);
  }

  render() {
    return (
      <div className="item">
        <div className="buttons">
          <span className="delete-btn" onClick={this.deletehandeler} />
          <span
            className={this.state.likeclass}
            onMouseEnter={this.overlike}
            onMouseLeave={this.leavelike}
          />
        </div>
        <div className="image">          
          <img src={require('../../Assist/item-1.png')} alt="" />        
          {/* <img src={require(`${this.props.item.image}`)} alt="" /> */}
        </div>
        
        <div className="description">
          <span>{this.props.item.name}</span>
          <span>{this.props.item.details}</span>
          <span>{this.props.item.color}</span>
        </div>
        <div className="quantity">
          <button
            className="plus-btn"
            onClick={() => this.handleIncrement(this.state.count)}
            type="button"
            name="button"
          >
            <img src={plus} alt="plus" />
          </button>
          <span className="counter">{this.state.count}</span>
          <button
            className="minus-btn"
            onClick={() =>
              this.handleDecrement(this.props.item.id, this.state.count)
            }
            type="button"
            name="button"
          >
            <img src={minus} alt="minus" />
          </button>
        </div>
        <div className="total-price">
          {this.Totallprice(this.props.item.product_price, this.state.count)}
        </div>
      </div>
    );
  }
}

export default Product;
