import React, { Component } from "react";
import { Link } from "react-router-dom";
import './Menu.css'

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (      
        <div className="menu">
          <ul>
            <li>
              <Link className="active" to="/">
                Home
              </Link>
            </li>
            <li>
              <Link to="/News">News</Link>
            </li>
            <li>
              <Link to="/Contact">Contact</Link>
            </li>
            <li>
              <Link to="About">About</Link>
            </li>
          </ul>
        </div>
    );
  }
}

export default Menu;
