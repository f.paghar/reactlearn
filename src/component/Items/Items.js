import React, { Component } from "react";
import "./Items.css";

import Product from "../../component/product/product";

import * as actionTypes from '../../Contaniers/Action/Action';
import { connect } from 'react-redux';

class App extends Component {
  // constructor(props) {
  //   super(props);
  // }


  render() {
    return (
      <div>     
          <div className="shopping-cart">
          <div className="title">Shopping Bag</div>
          {this.props.ctr.map((item_product, id) => {
            return (
              <Product item={item_product} action={this.props.deleteitem} key={id} />              
            );
          })}
        </div>
      </div>
    );
  }
}

// export default App;

const mapStateToProps = state => {
  return {
      ctr: state.ctr.items    
  }
};

const mapDispatchToProps = dispatch => {
  return {        
    deleteitem: () => dispatch({type: actionTypes.DELETEITEMS,id:1})         
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
